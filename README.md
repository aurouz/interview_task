## Overview

The purpose of this task is to let the candidate to demonstarte his/her capabilities in programming with:
- good programming practices in general
- informative comments
- unit tests
- etc

## Task

Hummingbot is an open source framework that helps building automated trading strategies, or bots that run on cryptocurrency exchanges.
Your task is to implement the interface that could integrate with Aurouz exchange.
A fork of hummingbot repository will be sent you seperately,
please commit you code under branch interview-[your_name].
You shall have one week to complete the task.
You may go ahead to create an account at https://app.aurouz-test.com and create an API key in order to perform an integration test.
You shall demonstrate your work that is able to integrate with Aurouz Exchange and able to perform market-making operations.
Please let us know if you have any questions.

### Aurouz API Documentation:
https://app.aurouz-test.com/exchange-documentation

### Hummingbot Github:
https://github.com/hummingbot/hummingbot





